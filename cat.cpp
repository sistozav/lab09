///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Vinton Sistoza <sistozav@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   4/29/21
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>
#include <queue>

#define CAT_NAMES_FILE "names.txt"
#define nameLen (6)

#include "cat.hpp"

using namespace std;

Cat::Cat( const std::string newName ) {
	setName( newName );
}


void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );

	name = newName;
}


std::vector<std::string> Cat::names;


void Cat::initNames() {
	names.clear();
	cout << "Loading... ";

	ifstream file( CAT_NAMES_FILE );
	string line;
	while (getline(file, line)) names.push_back(line);

	cout << to_string( names.size() ) << " names." << endl;
}


Cat* Cat::makeCat(){
	// Get a random cat from names
	auto nameIndex = rand() % names.size();
	auto name = names[nameIndex];

	// Remove the cat (by index) from names
	erase( names, names[nameIndex] );

	return new Cat( name );
}


void CatEmpire::catFamilyTree() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

dfsInorderReverse( topCat, 1 );
}



void CatEmpire::catList() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

dfsInorder( topCat );
}





void CatEmpire::catBegat() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}
dfsPreorder( topCat );
}

void CatEmpire::addCat( Cat* atCat, Cat* newCat ){
   if(atCat->name > newCat->name){
      if(atCat->left == nullptr)
         atCat->left = newCat;
      else
         addCat(atCat->left,newCat);
   }
   if(atCat->name < newCat->name){
      if(atCat->right == nullptr){
         atCat->right = newCat;
      }
      else{
         addCat(atCat->right,newCat);
      }
   }
}
void CatEmpire::addCat(Cat* newCat){
   newCat->left = nullptr;
   newCat->right = nullptr;
   if(topCat == nullptr){
      topCat = newCat;
      return;
   }
   addCat(topCat, newCat);
}
void CatEmpire::dfsInorderReverse(Cat* atCat, int depth) const{
   if(atCat == nullptr)
      return;
   dfsInorderReverse(atCat->right, depth+1);
   if(atCat->left == nullptr && atCat->right == nullptr){
      cout << string(nameLen * (depth-1), ' ' ) << atCat->name << endl;
   }
   else if(atCat->left != nullptr && atCat->right != nullptr){
      cout << string(nameLen * (depth-1), ' ' ) << atCat->name << "<" << endl;
   }
   else if(atCat->left != nullptr && atCat->right == nullptr){
      cout << string(nameLen * (depth-1), ' ' ) << atCat->name << "/" << endl;
   }
   else if(atCat->left == nullptr && atCat->right != nullptr){
      cout << string(nameLen * (depth-1), ' ' ) << atCat->name << "/" << endl;
   }
   dfsInorderReverse(atCat->left,depth+1);
}
void CatEmpire::dfsInorder(Cat* atCat) const{
   if(atCat == nullptr)
      return;
   dfsInorder(atCat->left);
   cout << atCat->name << endl;
   dfsInorder(atCat->right);
}
void CatEmpire::dfsPreorder(Cat* atCat) const{
   if(atCat == nullptr)
      return;
   if(atCat->left != nullptr && atCat->right != nullptr){
      cout << atCat->name << " begat " << atCat->left->name << " and " << atCat->right->name << endl;
   }
   else if(atCat->left != nullptr && atCat->right == nullptr){
      cout << atCat->name << " begat " << atCat->left->name << endl;
   }
   else if(atCat->left == nullptr && atCat->right != nullptr){
      cout << atCat->name << " begat " << atCat->right->name << endl;
   }
   else{//do nothing
   }
   dfsPreorder(atCat->left);
   dfsPreorder(atCat->right);
}
void CatEmpire::catGenerations() const{
   if(topCat == nullptr)
      cout << "No Cats!" << endl;

   BFS(topCat, 1);
}
void CatEmpire::BFS(Cat* atCat,int depth) const{
   long unsigned int i;
   if(atCat == nullptr)
      return;
   queue<Cat*> catQueue;
   catQueue.push(atCat);
   while(!catQueue.empty()){
      i = catQueue.size();
      getEnglishSuffix(depth);
      cout << "\t";
      for(long unsigned int j=0 ; j<i ; j++ ){
         Cat* cat = catQueue.front();
         cout << cat->name << "   ";
         if(cat == nullptr)
            return;
         if(cat->left != nullptr)
            catQueue.push(cat->left);
         if(cat->right != nullptr)
            catQueue.push(cat->right);
         catQueue.pop();
      }
      cout << endl;
      depth++;
   }
}
void CatEmpire::getEnglishSuffix(int n) const{
   if(n == 1)
      cout << "1st Generation" << endl;
   else if(n == 2)
      cout << "2nd Generation" << endl;
   else if(n == 3)
      cout << "3rd Generation" << endl;
   else
      cout << n << "th Generation" << endl;
}
